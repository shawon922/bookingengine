from rest_framework.generics import ListAPIView
from django.db.models import Q
from .models import BookingInfo, Reservation
from .paginations import CustomAPIPagination
from .serializers import BookingInfoSerializer


class BookingInfoAPIView(ListAPIView):
    queryset = BookingInfo.objects.all()
    serializer_class = BookingInfoSerializer
    pagination_class = CustomAPIPagination

    def get_queryset(self):
        max_price = self.request.query_params.get('max_price')
        check_in = self.request.query_params.get('check_in')
        check_out = self.request.query_params.get('check_out')

        queryset = super(BookingInfoAPIView, self).get_queryset()

        if max_price:
            queryset = queryset.filter(price__lte=max_price)

        if check_in and check_out:
            # Fetch all booking info ids whose overlaps the check_in and check_out range
            reserved_booking_ids = Reservation.objects.filter(
                Q(check_in__lte=check_in, check_out__gte=check_in)
                | Q(check_in__lte=check_out, check_out__gte=check_out)
                | Q(check_in__gte=check_in, check_out__lte=check_out)
            )\
            .values_list('booking_info_id',
                         flat=True)

            # Exclude the blocked ids in the date range
            queryset = queryset.exclude(pk__in=reserved_booking_ids)

        return queryset.order_by('price')

