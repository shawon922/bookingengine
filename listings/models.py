from django.db import models
from django.contrib.auth import get_user_model

UserModel = get_user_model()


class Listing(models.Model):
    HOTEL = 'hotel'
    APARTMENT = 'apartment'
    LISTING_TYPE_CHOICES = (
        ('hotel', 'Hotel'),
        ('apartment', 'Apartment'),
    )

    listing_type = models.CharField(
        max_length=16,
        choices=LISTING_TYPE_CHOICES,
        default=APARTMENT
    )
    title = models.CharField(max_length=255,)
    country = models.CharField(max_length=255,)
    city = models.CharField(max_length=255,)

    def __str__(self):
        return self.title
    

class HotelRoomType(models.Model):
    hotel = models.ForeignKey(
        Listing,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='hotel_room_types'
    )
    title = models.CharField(max_length=255,)

    def __str__(self):
        return f'{self.hotel} - {self.title}'


class HotelRoom(models.Model):
    hotel_room_type = models.ForeignKey(
        HotelRoomType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='hotel_rooms'
    )
    room_number = models.CharField(max_length=255,)

    def __str__(self):
        return self.room_number


class BookingInfo(models.Model):
    listing = models.OneToOneField(
        Listing,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='booking_info'
    )
    hotel_room_type = models.OneToOneField(
        HotelRoomType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='booking_info',
    )
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        if self.listing:
            obj = self.listing
        else:
            obj = self.hotel_room_type
            
        return f'{obj} {self.price}'

    # Listing type (i.e. Apartment or Hotel)
    def listing_type(self):
        if self.hotel_room_type:
            obj = self.hotel_room_type.hotel
        else:
            obj = self.listing

        if obj:
            return f'{obj.listing_type.title()}'
        return None

    # Title of hotel room type
    def title(self):
        if self.hotel_room_type:
            obj = self.hotel_room_type.hotel
        else:
            obj = self.listing

        if obj:
            return f'{obj.title}'

        return None

    def country(self):
        if self.hotel_room_type:
            obj = self.hotel_room_type.hotel
        else:
            obj = self.listing

        if obj:
            return f'{obj.country}'
        return None

    def city(self):
        if self.hotel_room_type:
            obj = self.hotel_room_type.hotel
        else:
            obj = self.listing

        if obj:
            return f'{obj.city}'
        return None


class Reservation(models.Model):
    ACTIVE = 'active'
    CANCELLED = 'cancelled'
    RESERVATION_STATUS_CHOICES = (
        ('active', 'Active'),
        ('cancelled', 'Cancelled'),
    )

    user = models.ForeignKey(
        UserModel,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='reservations'
    )
    booking_info = models.ForeignKey(
        BookingInfo,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='reservations'
    )
    check_in = models.DateField()
    check_out = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50,
                              choices=RESERVATION_STATUS_CHOICES,
                              default=ACTIVE)

    def __str__(self):
        if self.booking_info:
            return f'Reservation: {self.booking_info} {self.check_in} - {self.check_out}'

