from django.urls import path

from listings.views import BookingInfoAPIView

app_name = 'listings'

urlpatterns = [
    path('units/', BookingInfoAPIView.as_view(), name='booking_info')
]