from datetime import datetime, timedelta
from django.urls import reverse
from rest_framework import status
from rest_framework.test import (
    APITestCase,
    APIClient,
)
from .models import (
    Listing,
    HotelRoom,
    HotelRoomType,
    BookingInfo,
    Reservation,
)

client = APIClient()


def create_listing(listing_type, title, country, city):
    """
    Create listing
    """
    return Listing.objects.create(listing_type=listing_type,
                                  title=title,
                                  country=country,
                                  city=city)


def create_room_type(listing, title):
    """
    Create hotel room type
    """
    return HotelRoomType.objects.create(hotel=listing,
                                        title=title)


def create_room(room_type, room_number):
    """
    Create hotel room
    """
    return HotelRoom.objects.create(hotel_room_type=room_type,
                                    room_number=room_number)


def create_booking_info(price, listing=None, hotel_room_type=None):
    """
    Create booking info
    """
    return BookingInfo.objects.create(price=price,
                                      listing=listing,
                                      hotel_room_type=hotel_room_type)


def create_reservation(booking_info,
                       check_in,
                       check_out):
    """
    Create reservation
    """
    return Reservation.objects.create(booking_info=booking_info,
                                      check_in=check_in,
                                      check_out=check_out)


class BookingInfoTest(APITestCase):
    def setUp(self):
        room_types = ['Single Room', 'Double Room', 'Triple Room']
        self.api_url = reverse('listings:booking_info')
        self.listing = create_listing(listing_type='hotel', title='Hotel Lux 5***', country='UK', city='London')

        price_list = [50, 60, 200]

        for index, room_type in enumerate(room_types):
            room_type = create_room_type(listing=self.listing, title=room_type)
            create_room(room_type=room_type, room_number=f'{index+1}01')
            booking_info = create_booking_info(price=price_list[index], hotel_room_type=room_type)

            # Blocking a room which price is 50
            if booking_info.price == 50:
                check_in = datetime.now() + timedelta(days=2)
                check_out = datetime.now() + timedelta(days=4)
                create_reservation(booking_info=booking_info,
                                   check_in=check_in.date(),
                                   check_out=check_out.date())

    def test_booking_info_list(self):
        response = client.get(self.api_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('items')), 3)

    def test_booking_info_max_price_fifty(self):
        response = client.get(self.api_url, data={'max_price': 50})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('items')), 1)

    def test_booking_info_max_price_sixty(self):
        response = client.get(self.api_url, data={'max_price': 60})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('items')), 2)

    def test_booking_info_max_price_two_hundred(self):
        response = client.get(self.api_url, data={'max_price': 200})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('items')), 3)

    def test_booking_info_in_blocked_date_range(self):
        check_in = datetime.now() + timedelta(days=3)
        check_out = datetime.now() + timedelta(days=5)

        response = client.get(self.api_url, data={'max_price': 50,
                                                  'check_in': check_in.date(),
                                                  'check_out': check_out.date()})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # No items will be returned. Room with price 50 is already blocked
        self.assertEqual(len(response.json().get('items')), 0)

    def test_booking_info_in_without_blocked_date_range(self):
        check_in = datetime.now() + timedelta(days=6)
        check_out = datetime.now() + timedelta(days=8)

        response = client.get(self.api_url, data={'max_price': 50,
                                                  'check_in': check_in.date(),
                                                  'check_out': check_out.date()})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # 1 item will be returned. Room with price 50 is not blocked in the date range
        self.assertEqual(len(response.json().get('items')), 1)
