from rest_framework.serializers import ModelSerializer
from .models import BookingInfo, Reservation


class BookingInfoSerializer(ModelSerializer):

    class Meta:
        model = BookingInfo
        fields = ('listing_type', 'title', 'country', 'city', 'price', )
